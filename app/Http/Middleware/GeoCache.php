<?php

namespace App\Http\Middleware;

use Closure;
use Cache;

class GeoCache
{
    public function handle($request, Closure $next)
    {
        $return = $next($request);
        if ($request->has('ip')) {
            $key = config('constants.ipgeo_prefix') . $request->ip;

            if (Cache::has($key))
                $return = Cache::get($key);
            else if ($return->getStatusCode() == config('constants.ipgeo_status'))
                Cache::put($key, $return, config('constants.ipgeo_ttl'));
        }

        return $return;
    }
}
