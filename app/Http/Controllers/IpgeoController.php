<?php

namespace App\Http\Controllers;

use App\Ipgeo;
use Illuminate\Http\Request;

class IpgeoController extends Controller
{
    public function get(Request $request)
    {
        $this->validate($request, [
            'ip' => 'required|ipv4'
        ]);

        return Ipgeo::where('ip', $request->ip)->firstOrFail()->getIPGeo();
    }
}