<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ipgeo extends Model
{
    public function locality()
    {
        return $this->belongsTo('App\Locality');
    }

    public function getIPGeo()
    {
        return [
            'ip' => $this->ip,
            'country' => $this->locality->country->name,
            'locality' => $this->locality->name,
            'lat' => $this->locality->lat,
            'lng' => $this->locality->lng
        ];
    }
}
