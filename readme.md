lumen + mysql + redis + docker

## Установка

1. Откройте консоль и выполните в папке laradock команду docker-compose up -d nginx redis mysql
2. В workspace bash контейнера (docker-compose exec workspace bash) выполнить:
	- composer install
	- php artisan migrate
	- php artisan db:seed
	
3. Откройте в браузере localhost

#### Требования для работы вне Docker окружения

- PHP 7 или выше
- MySql 5.7 или выше
- Apache или Nginx