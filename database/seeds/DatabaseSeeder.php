<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        for ($i = 0; $i < 10; $i++) {
            $country = DB::table('countries')->insertGetId([
                'name' => $faker->country
            ]);

            for ($j = 0; $j < 10; $j++) {
                $locality = DB::table('localities')->insertGetId([
                    'name' => $faker->city,
                    'lat' => $faker->latitude,
                    'lng' => $faker->longitude,
                    'country_id' => $country
                ]);

                for ($k = 0; $k < 10; $k++) {
                    $firstDigits = $faker->numberBetween(1, 255) . '.' . $faker->numberBetween(1, 255) . '.';
                    DB::table('ipgeos')->insert([
                        'ip' => $firstDigits . $faker->numberBetween(1, 255) . '.' . $faker->numberBetween(1, 255),
                        'locality_id' => $locality
                    ]);
                }
            }
        }
    }
}
